#!/usr/local/bin/python3
import os
import sys
import json
import time
import argparse
import requests
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions


class Check:
    def __init__(self,
                 config,
                 store,
                 product,
                 url,
                 scraper='python-requests',
                 look_for = None,
                 in_stock_if_found = None,
                 speak_count_if_found = 1,
                 headless = True,
                 extra_page_load_time = 0):

        self.config = config
        self.store = store
        self.product = product
        self.url = url
        self.scraper = scraper
        self.look_for = look_for
        self.in_stock_if_found = in_stock_if_found
        self.speak_count_if_found = speak_count_if_found
        self.headless = headless
        self.extra_page_load_time = extra_page_load_time


    def __log(self, s):
        now = datetime.now()
        dt_string = now.strftime('%d/%m/%Y %H:%M:%S')
        print('[%s] [%s] %s :: %s' % (dt_string, self.store, self.product, s))

    def __speak(self, something):
        for i in range(0, self.speak_count_if_found):
            self.__log('speaking "%s"' % something)
            os.system('say "%s"' % something)
            if self.speak_count_if_found > 1:
                time.sleep(1)

    def check(self):
        if self.store.lower() == 'target':
            self.check_target()
        else:
            self.check_generic()

    def __wait_for_element(self, driver, selector):
        # self.__log('waiting for selector: %s' % selector)
        start_time = time.time()
        timeout = 15
        while True:
            try:
                e = driver.find_element_by_css_selector(selector)
                return e
            except Exception:
                if time.time() > start_time + timeout:
                    raise Exception('timed out while waiting for selector: %s' % selector)
                time.sleep(0.1)

    def check_generic(self):
        if self.scraper == 'python-requests':
            r = requests.get(self.url, headers=self.config['headers'])
            # if show_html:
            #     print(r.text)
            lines = r.text.split('\n')
            for i in range(0, len(lines)):
                found_all = True
                line = lines[i].lower()
                for word in self.look_for:
                    if word not in line:
                        found_all = False
                if found_all:
                    break

        elif self.scraper in ['Chrome', 'Firefox']:

            if self.scraper == 'Chrome':
                options = ChromeOptions()
                options.add_argument('user-data-dir=/Users/chris/Dropbox/PS5/chrome_cache')
                if self.headless:
                    options.add_argument('--headless')
                driver = webdriver.Chrome(options=options, executable_path='/Users/chris/Dropbox/PS5/chromedriver')
            else:
                options = FirefoxOptions()
                profile = webdriver.FirefoxProfile()
                profile.set_preference('browser.cache.disk.enable', True)
                profile.set_preference('browser.cache.memory.enable', True)
                profile.set_preference('browser.cache.offline.enable', True)
                if self.headless:
                    options.add_argument('-headless')
                driver = webdriver.Firefox(options=options, executable_path='/Users/chris/Dropbox/PS5/geckodriver')

            found_all = True
            driver.get(self.url)
            time.sleep(self.extra_page_load_time)

            # block on capthca
            notified_capthca = False
            while True:
                found_capthca = False
                captcha_selectors = [
                    # generic
                    '.goog-inline-block',

                    # WalMart
                    'p.bot-message',

                    # Playstation Direct
                    'div#MainPart_divWarningBox'
                ]

                for captcha_selector in captcha_selectors:
                    try:
                        elem = driver.find_element_by_css_selector(captcha_selector)
                        found_capthca = True
                        if not notified_capthca:
                            self.__log('found %s' % captcha_selector)
                            self.__speak('blocked by captcha', times=1)
                            notified_capthca = True
                    except Exception:
                        pass

                if not found_capthca:
                    break

            if found_capthca:
                self.__speak('no longer blocked by captcha', times=1)

            for word in self.look_for:
                try:
                    elem = driver.find_element_by_css_selector(word)
                    self.__log('found %s' % word)
                except:
                    found_all = False
            driver.close()

        if self.in_stock_if_found and found_all:
            self.__log('IN STOCK')
            self.__speak('in stock')
        elif self.in_stock_if_found and not found_all:
            self.__log('OUT OF STOCK')
        elif not self.in_stock_if_found and found_all:
            self.__log('OUT OF STOCK')
        elif not self.in_stock_if_found and not found_all:
            self.__log('possible stock')
            self.__speak('possible stock')

    def check_target(self):
        options = FirefoxOptions()
        profile = webdriver.FirefoxProfile()
        # profile.set_preference('browser.cache.disk.enable', True)
        # profile.set_preference('browser.cache.memory.enable', True)
        # profile.set_preference('browser.cache.offline.enable', True)
        if self.headless:
            options.add_argument('-headless')
        driver = webdriver.Firefox(options=options, executable_path='/Users/chris/Dropbox/PS5/geckodriver')
        driver.get(self.url)
        try:
            for zip in [
                '78619',    # center
                '77833',    # Brenham (east)
                # '78624',    # Fredericksberg
                '76701',    # Waco (north)
                '78201'     # San Antonio
            ]:
                # self.__log('zip=%s' % zip)
                webdriver.ActionChains(driver).send_keys(Keys.ESCAPE).perform()
                time.sleep(1)

                e = self.__wait_for_element(driver, 'button[data-test="fiatsButton"]')
                e.click()

                # self.__log('editing location')
                e = self.__wait_for_element(driver, 'a[data-test="storeSearchLink"]')
                e.click()

                # self.__log('clearing input')
                e = self.__wait_for_element(driver, 'input[data-test="fiatsLocationInput"]')
                e.clear()

                # self.__log('setting %s' % zip)
                e = self.__wait_for_element(driver, 'input[data-test="fiatsLocationInput"]')
                e.send_keys(zip)

                # self.__log('clicking "Find stores"')
                e = self.__wait_for_element(driver, 'button[data-test="fiatsUpdateLocationSubmitButton"]')
                e.click()

                # self.__log('filtering for in stock only')
                e = self.__wait_for_element(driver, 'div.switch-track')
                e.click()
                time.sleep(1)

                found_stock = False

                # check for limited stock
                try:
                    selector = 'div[data-test="storeAvailabilityStoreCard"] span.h-text-bold.h-text-orangeDark'
                    e = driver.find_element_by_css_selector(selector)
                    found_stock = True
                    self.__speak('%s limited stock at %s' % (self.product, self.store))
                except Exception:
                    pass

                # check for in stock
                try:
                    selector = 'div[data-test="storeAvailabilityStoreCard"] span.h-text-bold.h-text-greenDark'
                    e = driver.find_element_by_css_selector(selector)
                    found_stock = True
                    self.__speak('%s in stock at %s' % (self.product, self.store))
                except Exception:
                    pass

                if found_stock:
                    self.__log('IN STOCK')
                    text = '%s stock alert at %s' % (self.product, self.store)
                    command = 'curl -s -X POST -H \'Content-type: application/json\' --data \'{"text":"%s!"}\' https://hooks.slack.com/services/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' % text
                    os.system(command)
                else:
                    self.__log('OUT OF STOCK')

            time.sleep(5)
        except Exception as e:
            print('not found: %s' % str(e))
            sys.exit(0)

        # time.sleep(10)


        driver.close()


def gamestop():
    log('checking Gamestop')

    # digital
    url = 'https://www.gamestop.com/video-games/playstation-5/consoles/products/playstation-5-digital-edition/11108141.html?condition=New'
    #check(url, ['add-to-cart'], speak='Gamestop')

    # Spider-Man
    url = 'https://www.gamestop.com/video-games/playstation-5/games/products/marvels-spider-man-miles-morales-launch-edition/11108199.html'

    #from selenium.webdriver.chrome.options import Options
    from selenium import webdriver
    options = webdriver.ChromeOptions()
    #chrome_options = Options()
    #chrome_options.add_argument('--headless')
    options.add_argument('--incognito')
    driver = webdriver.Chrome(options=options, executable_path='/Users/chris/Desktop/chromedriver')
    driver.get(url)
    print(str(driver.find_element_by_class_name('#px-captcha')))


def validate_config(config):
    def verify_name():
        pass
    def verify_url():
        pass
    def verify_scraper():
        pass
    def verify_look_for():
        pass


if __name__ == '__main__':
    try:
        with open('ps5_config.json', 'r') as f:
            config = json.loads(f.read())
    except Exception as e:
        pass

    try:
        validate_config(config)
    except Exception as e:
        pass

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--all', action='store_true')
        parser.add_argument('--amazon', action='store_true')
        parser.add_argument('--antonline', action='store_true')
        parser.add_argument('--bestbuy', action='store_true')
        parser.add_argument('--costco', action='store_true')
        parser.add_argument('--gamestop', action='store_true')
        parser.add_argument('--playstation-direct', action='store_true')
        parser.add_argument('--target', action='store_true')
        parser.add_argument('--target-search', action='store_true')
        parser.add_argument('--walmart', action='store_true')
        parser.add_argument('--test', action='store_true')
        args = parser.parse_args()
    except Exception as e:
        print(str(e))
        sys.exit(1)

    checks = []
    checking_stores = []

    if args.test:
        print('TEST MODE')
        checking_stores = ['test']
    else:
        # if args.all or args.amazon:
        #     checking_stores.append('amazon')
        if args.all or args.antonline:
            checking_stores.append('antonline')
        if args.all or args.bestbuy:
            checking_stores.append('bestbuy')
        if args.all or args.costco:
            checking_stores.append('costco')
        # if args.all or args.gamestop:
        #     checking_stores.append('gamestop')
        # if args.all or args.playstation_direct:
        #     checking_stores.append('playstation_direct')
        if args.all or args.target:
            checking_stores.append('target')
        # if args.all or args.target_search:
        #     checking_stores.append('target (search page)')
        #     target_search_checks = config['sites']['target-search']
        if args.all or args.walmart:
            checking_stores.append('walmart')

        # print('checking the following stores: %s' % ', '.join(checking_stores))
    for store in checking_stores:
        if store in config['stores']:
            checks += config['stores'][store]

    # print('number of checks: %d' % len(checks))

    for c in checks:
        try:
            checker = Check(config, c['store'], c['product'], c['url'])
            if 'scraper' in c: checker.scraper = c['scraper']
            if 'headless' in c: checker.headless = c['headless']
            if 'look_for' in c: checker.look_for = c['look_for']
            if 'in_stock_if_found' in c: checker.in_stock_if_found = c['in_stock_if_found']
            if 'speak_count_if_found' in c: checker.speak_count_if_found = c['speak_count_if_found']
            if 'extra_page_load_time' in c: checker.extra_page_load_time = c['extra_page_load_time']
            checker.check()
        except Exception as e:
            print(str(e))
